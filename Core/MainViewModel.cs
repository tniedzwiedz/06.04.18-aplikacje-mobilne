﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Core.Annotations;

namespace Core
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private string _password;
        private string _login;
        public event PropertyChangedEventHandler PropertyChanged;

        public MainViewModel()
        {
            VerifyDataCollection = new ObservableCollection<string>();
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Login
        {
            get => _login;
            set
            {
                if (value == _login) return;
                _login = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (value == _password) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> VerifyDataCollection { get; }

        public int SelectedIndex
        {
            get => -1;
            set => VerifyDataCollection.RemoveAt(value);
        }

        public ICommand VerifyDataCommand => new Command(() =>
        {
            if(_login == "a" & _password == "b")
            VerifyDataCollection.Add("You Logged In");

            else
            {
                VerifyDataCollection.Add("Wrong login or password");
            }
        });
    }
    public class Command : ICommand
    {
        private readonly Action _action;
        public Command(Action action)
        {
            _action = action;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            _action.Invoke();
        }
        public event EventHandler CanExecuteChanged;
    }

}

